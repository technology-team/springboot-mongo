package net.suby.springboottrino.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import net.suby.springboottrino.mongo.model.Account;

public interface AccountRepository extends MongoRepository<Account, Long> {
}
