package net.suby.springboottrino.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import net.suby.springboottrino.mongo.model.Member;

public interface MemberRepository extends MongoRepository<Member, Long> {
}
