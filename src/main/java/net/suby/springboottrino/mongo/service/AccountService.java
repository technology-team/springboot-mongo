package net.suby.springboottrino.mongo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import net.suby.springboottrino.mongo.dto.AccountDto;
import net.suby.springboottrino.mongo.model.Account;
import net.suby.springboottrino.mongo.repository.AccountRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;

    public void create(AccountDto accountDto) {
        Account account = Account.builder()
                                 .id(accountDto.getId())
                                 .name(accountDto.getName())
                                 .address(accountDto.getAddress())
                                 .build();
        accountRepository.save(account);
    }

    public List<Account> findAll() {
        return accountRepository.findAll();
    }

    public Account findById(long id) {
        return accountRepository.findById(id).orElseGet(Account::new);
    }

    public void deleteById(long id) {
        accountRepository.deleteById(id);
    }
}
