package net.suby.springboottrino.mongo.service;

import org.springframework.stereotype.Service;

import net.suby.springboottrino.mongo.dto.AccountDto;
import net.suby.springboottrino.mongo.model.Member;
import net.suby.springboottrino.mongo.repository.MemberRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class MemberService {

    private final MemberRepository memberRepository;

    public void create(AccountDto memberDto) {
        Member member = Member.builder()
                              .id(memberDto.getId())
                              .name(memberDto.getName())
                              .address(memberDto.getAddress())
                              .build();
        memberRepository.save(member);
    }
}
