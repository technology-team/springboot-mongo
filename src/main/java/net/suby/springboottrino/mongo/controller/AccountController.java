package net.suby.springboottrino.mongo.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.suby.springboottrino.mongo.dto.AccountDto;
import net.suby.springboottrino.mongo.model.Account;
import net.suby.springboottrino.mongo.service.AccountService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/accounts")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @PostMapping
    public void create(@RequestBody AccountDto accountDto) {
        accountService.create(accountDto);
    }

    @GetMapping
    public List<Account> findAll() {
        return accountService.findAll();
    }

    @GetMapping("/{id}")
    public Account findById(@PathVariable("id") long id) {
        return accountService.findById(id);
    }

    @DeleteMapping("/{id}")
    public String deleteById(@PathVariable("id") long id) {
        accountService.deleteById(id);
        return "Success Deleted";
    }
}
