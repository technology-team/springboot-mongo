package net.suby.springboottrino.mongo.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.suby.springboottrino.mongo.dto.AccountDto;
import net.suby.springboottrino.mongo.service.MemberService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/members")
@RequiredArgsConstructor
public class MemberController {

    private final MemberService memberService;

    @PostMapping
    public void create(@RequestBody AccountDto memberDto) {
        memberService.create(memberDto);
    }
}
