package net.suby.springboottrino.mongo.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AccountDto {
    private Long id;

    private String name;

    private String address;
}
