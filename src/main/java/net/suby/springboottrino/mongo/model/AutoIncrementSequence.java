package net.suby.springboottrino.mongo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Document(collection = "auto_sequence")
@Setter
@Getter
public class AutoIncrementSequence {

    @Id
    private String id;

    private long seq;
}
