package net.suby.springboottrino.mongo.listner;

import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;

import net.suby.springboottrino.mongo.model.Account;
import net.suby.springboottrino.mongo.service.SequenceGeneratorService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class AccountListener extends AbstractMongoEventListener<Account> {

    private final SequenceGeneratorService generatorService;

    @Override
    public void onBeforeConvert(BeforeConvertEvent<Account> event) {    // 모델별로 만들지 않고 인터페이스를 통해 공용의 시퀸스 리스너를 만들 수 있을것으로 판단된다.
        event.getSource().setId(generatorService.generateSequence(Account.SEQUENCE_NAME));
    }
}
