package net.suby.springboottrino.mongo.listner;

import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.stereotype.Component;

import net.suby.springboottrino.mongo.model.AutoIncrementId;
import net.suby.springboottrino.mongo.service.SequenceGeneratorService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Component
public class AutoIncrementListener<T extends AutoIncrementId> extends AbstractMongoEventListener<T> {

    private final SequenceGeneratorService generatorService;

    @Override
    public void onBeforeConvert(BeforeConvertEvent<T> event) {
        event.getSource().setId(generatorService.generateSequence(event.getSource().getSequenceName()));
    }
}
